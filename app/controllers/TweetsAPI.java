package controllers;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import models.Tweet;
import play.mvc.Controller;

public class TweetsAPI extends Controller
{
  static Gson gson = new Gson();

  public static void createTweet(String id, JsonElement body)
  {
    Tweet tweet = gson.fromJson(body.toString(), Tweet.class);
    tweet.save();
    renderJSON(gson.toJson(tweet));
  }
}