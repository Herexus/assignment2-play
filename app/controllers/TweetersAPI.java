package controllers;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import models.Tweet;
import models.Tweeter;
import play.mvc.Controller;

public class TweetersAPI extends Controller
{
  static Gson gson = new Gson();

  public static void getAllTweeters()
  {
    List<Tweeter> Tweeters = Tweeter.findAll();
    renderJSON(gson.toJson(Tweeters));
  }

  public static void createTweeter(JsonElement body)
  {
    Tweeter tweeter = gson.fromJson(body.toString(), Tweeter.class);
    tweeter.save();
    renderJSON(gson.toJson(tweeter));
  }

}