package models;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import play.db.jpa.GenericModel;
import play.db.jpa.Model;

@Entity
public class Tweet extends GenericModel
{
  @Id
  public UUID id;
  public Date  date;
  public String tweetout;
  public String  tweetwho;
  public int charCount;
/*  public String id;
  public String message;
  public String datestamp;*/

}